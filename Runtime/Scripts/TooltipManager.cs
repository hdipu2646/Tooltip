using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Walid.UI
{
    public class TooltipManager : MonoBehaviour
    {
        #region Variables
        [Tooltip("ToolTips Viewing and Hiding Times")]
        [Header("ToolTips Viewing and Hiding Times")]
        public float viewTime;
        public float hideRatio;

        [Tooltip("ToolTips")]
        [Header("ToolTips")]
        [SerializeField] private RectTransform bottomRightTip;
        [SerializeField] private RectTransform bottomLeftTip, topRightTip, topLeftTip;

        [Tooltip("Currently Activated Tooltip")]
        [Header("Currently Activated Tooltip")]
        private RectTransform activeToolTip;
        #endregion Variables

        #region Singleton
        private static TooltipManager _instance;
        public static TooltipManager Singleton
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<TooltipManager>();
                    if (_instance == null)
                    {
                        GameObject tooltipManager = new GameObject();
                        tooltipManager.name = typeof(TooltipManager).Name;
                        _instance = tooltipManager.AddComponent<TooltipManager>();
                        DontDestroyOnLoad(tooltipManager);
                    }
                }
                return _instance;
            }
        }

        public static void SetNewSingleTon(TooltipManager accessToolTipAccessManager)
        {
            if (_instance != null)
            {
                return;
            }
            _instance = accessToolTipAccessManager;
        }
        #endregion Singleton

        #region Public Methods
        /// <summary>
        /// Hiding & Stop tooltip by this Function
        /// </summary>
        public void HideToolTip()
        {
            if (activeToolTip is not null)
            {
                activeToolTip.gameObject.SetActive(false);
                StopAllCoroutines();
            }
        }

        /// <summary>
        /// Showing Tooltip on Mouse Position
        /// </summary>
        /// <param name="tooltipMessage">passing the tooltip message with it</param>
        public void ShowToolTip(string tooltipMessage)
        {
            HideToolTip();
            Vector3 viewPortPostion = Camera.main.ScreenToViewportPoint(Mouse.current.position.ReadValue());
            if (viewPortPostion.x < 0.5F && viewPortPostion.y < 0.5F)
                activeToolTip = topRightTip;
            else if (viewPortPostion.x > 0.5F && viewPortPostion.y < 0.5F)
                activeToolTip = topLeftTip;
            else if (viewPortPostion.x < 0.5F && viewPortPostion.y > 0.5F)
                activeToolTip = bottomRightTip;
            else
                activeToolTip = bottomLeftTip;
            activeToolTip.position = Mouse.current.position.ReadValue();
            ActivateTooltip(tooltipMessage);
        }

        /// <summary>
        /// Showing Tooltip on Mouse Position
        /// </summary>
        /// <param name="tooltipMessage">passing the tooltip message with it</param>
        /// <param name="tooltipPosition">passing the tooltip tooltip Position from outside</param>
        public void ShowToolTip(string tooltipMessage, Vector3 tooltipPosition)
        {
            HideToolTip();
            Vector3 viewPortPostion = Camera.main.ScreenToViewportPoint(tooltipPosition);
            if (viewPortPostion.x < 0.5F && viewPortPostion.y < 0.5F)
                activeToolTip = topRightTip;
            else if (viewPortPostion.x > 0.5F && viewPortPostion.y < 0.5F)
                activeToolTip = topLeftTip;
            else if (viewPortPostion.x < 0.5F && viewPortPostion.y > 0.5F)
                activeToolTip = bottomRightTip;
            else
                activeToolTip = bottomLeftTip;
            activeToolTip.position = tooltipPosition;
            ActivateTooltip(tooltipMessage);
        }
        #endregion Public Methods

        #region Private methods

        private void ActivateTooltip(string tooltipText)
        {
            activeToolTip.GetComponentInChildren<Text>().text = tooltipText;
            StartCoroutine(AnimateToolTip());
        }

        private IEnumerator AnimateToolTip()
        {
            activeToolTip.gameObject.SetActive(true);
            Graphic[] graphics = activeToolTip.GetComponentsInChildren<Graphic>();
            int length = graphics.Length;
            Color color;
            for (int i = 0; i < length; i++)
            {
                color = graphics[i].color;
                color.a = 1;
                graphics[i].color = color;
            }
            yield return new WaitForSeconds(viewTime);
            for (float alpha = 1; alpha >=0; alpha -= hideRatio)
            {
                for (int i = 0; i < length; i++)
                {
                    color = graphics[i].color;
                    color.a = alpha;
                    graphics[i].color = color;
                }
                yield return new WaitForEndOfFrame();
            }
            activeToolTip.gameObject.SetActive(false);
        }
        #endregion Private methods
    }
}