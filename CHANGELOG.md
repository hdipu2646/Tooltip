[v1.0.4](#v1.0.4)
### Added
- Added Script Icon
- Upgrade New Input System Code
- Added Dependency Packages -

    `com.unity.ugui": "1.0.0`

    `com.unity.inputsystem": "1.5.1`


### Changes
- Name Space Name to Walid.UI


[v1.0.3](#v1.0.3)
### Last Old Unity Input System version

[v1.0.2](#v1.0.2)

[v1.0.1](#v1.0.1)

[v1.0.0](#v1.0.0)